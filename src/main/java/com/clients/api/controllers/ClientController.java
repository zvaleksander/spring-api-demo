package com.clients.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.clients.api.models.Client;
import com.clients.api.services.ClientService;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "http://localhost:4200")
public class ClientController {

	@Autowired
	private ClientService clientService;

	@GetMapping(value = "/clients")
	@ResponseStatus(code = HttpStatus.OK)
	public Page<Client> index(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "size", defaultValue = "10") int size) {

		return clientService.findAll(page, size);
	}

	@GetMapping(value = "/clients/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Client show(@PathVariable(value = "id", required = true) Long id) {
		return clientService.findOne(id);
	}

	@PostMapping(value = "/clients")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Client store(@RequestBody Client client) {
		return clientService.save(client);
	}

	@PutMapping(value = "/clients/{id}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Client update(@RequestBody Client client, @PathVariable(value = "id", required = true) Long id) {
		Client _client = clientService.findOne(id);
		_client.setDocument(client.getDocument());
		_client.setName(client.getName());
		_client.setLastname(client.getLastname());
		_client.setEmail(client.getEmail());
		_client.setPhone(client.getPhone());
		_client.setAddress(client.getAddress());
		
		return clientService.save(_client);
	}
}
