package com.clients.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringClientsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringClientsApiApplication.class, args);
	}

}
