package com.clients.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.clients.api.models.Client;
import com.clients.api.repositories.ClientRepository;

@Service
public class ClientService {

	@Autowired
	private ClientRepository clientRepository;
	
	@Transactional(readOnly = true)
	public Page<Client> findAll(int page, int size) {
		Pageable pageable = PageRequest.of(page, size, Sort.by("id").descending());
		
		return clientRepository.findAll(pageable);
	}
	
	@Transactional(readOnly = true)
	public Client findOne(Long id) {
		return clientRepository.findById(id).orElse(null);
	}
	
	@Transactional
	public Client save(Client client) {
		return clientRepository.save(client);
	}
}
